package com.example.tel_c.sisfodesa.model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class KabarDesa extends RealmObject {

    @PrimaryKey
    private int id_kabardesa;
    private String judul;
    private Date tgl;
    private String isi;
    private Admin admin;

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public int getId_kabardesa() {
        return id_kabardesa;
    }

    public void setId_kabardesa(int id_kabardesa) {
        this.id_kabardesa = id_kabardesa;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public Date getTgl() {
        return tgl;
    }

    public void setTgl(Date tgl) {
        this.tgl = tgl;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
