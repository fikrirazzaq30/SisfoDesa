package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class RpjmDesa extends RealmObject {

    @PrimaryKey
    private int id_rpjmdesa;
    private String nama;
    private String tujuan;
    private double biaya;
    private String tahun;

    public int getId_rpjmdesa() {
        return id_rpjmdesa;
    }

    public void setId_rpjmdesa(int id_rpjmdesa) {
        this.id_rpjmdesa = id_rpjmdesa;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public double getBiaya() {
        return biaya;
    }

    public void setBiaya(double biaya) {
        this.biaya = biaya;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }
}
