package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class SejarahPemimpin extends RealmObject {

    @PrimaryKey
    private int id_pemimpin;
    private String nama;
    private String periode;
    private String keterangan;
    private SejarahDetil sejarahDetil;

    public SejarahDetil getSejarahDetil() {
        return sejarahDetil;
    }

    public void setSejarahDetil(SejarahDetil sejarahDetil) {
        this.sejarahDetil = sejarahDetil;
    }

    public int getId_pemimpin() {
        return id_pemimpin;
    }

    public void setId_pemimpin(int id_pemimpin) {
        this.id_pemimpin = id_pemimpin;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
